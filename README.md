# Server Platform POMs

## Purpose

These POMs are for Server products and plugins to import into their own POMs, to reduce the amount of dependency maintenance required.

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Usage

In your project's parent pom, import via dependency management:

    <properties>
        <platform.version>5.0.0</platform.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>platform</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>third-party</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

In your modules, import the desired dependencies with a scope, but not a version e.g.

    <dependencies>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

## Atlassian Developer?

### Adding new artifacts

If you are adding a new artifact to `platform` or `third-party` please make sure you will add it also to set of platform dependencies in [AMPS](https://bitbucket.org/atlassian/amps/src/master/): `com.atlassian.maven.plugins.amps.BannedDependencies#PLATFORM_DEPENDENCIES`.  
This set is used to prevent platform artifacts being bundled into plugins.

### Committing Guidelines

Please include at least one owner of this project on your pull request.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/JPP)

### Releasing

Releasing is done with a release stage in the bamboo CI build.

### Notes On Version 4.0

Platform version 4.0.0 and 4.0.1 were experimental builds of the platform containing Velocity 1.7 capable modules. There are no plans to adopt these modules, so the branch has been renamed to 4.0.x-dead. These changes are not present in master.

## External User?

### Issues

Please report any issues you find in [EAN](https://ecosystem.atlassian.net/browse/PLATFORM).
